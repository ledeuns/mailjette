# Mailjette

Mailjette is a self-hosted temporary email address service usable with cURL.

- `curl example.com/<address>/list` to get a list of emails (and `mailid`) sent to `<address>`
- `curl example.com/<address>/view/<mailid>` to view the email identifed with `<mailid>`

#### Usage :

```
$ curl http://mailjette.example.com/denis/list
[{"id": "60be4f75dc2a45c6c0fdd10c", "date": "2021-06-07T16:55:17.313000", "from": "signup@service.example.com"}]

$ curl http://mailjette.example.com/denis@mailjette.example.com/list
[{"id": "60be4f75dc2a45c6c0fdd10c", "date": "2021-06-07T16:55:17.313000", "from": "signup@service.example.com"}]

$ curl http://mailjette.example.com/denis/view/60be4f75dc2a45c6c0fdd10c
<div dir="ltr"><div>Ceci est un test</div><div><br></div><div>😃<br></div><br></div>
```

Fork me at https://framagit.org/ledeuns/mailjette.
