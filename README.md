# Mailjette

Mailjette is a self-hosted temporary email address service usable with cURL.

- `curl example.com/<address>/list` to get a list of emails (and `mailid`) sent to `<address>`
- `curl example.com/<address>/view/<mailid>` to view the email identifed with `<mailid>`

#### Usage :

```
$ curl http://mailjette.example.com/denis/list
[{"id": "60be4f75dc2a45c6c0fdd10c", "date": "2021-06-07T16:55:17.313000", "from": "signup@service.example.com"}]

$ curl http://mailjette.example.com/denis@mailjette.example.com/list
[{"id": "60be4f75dc2a45c6c0fdd10c", "date": "2021-06-07T16:55:17.313000", "from": "signup@service.example.com"}]

$ curl http://mailjette.example.com/denis/view/60be4f75dc2a45c6c0fdd10c
<div dir="ltr"><div>Ceci est un test</div><div><br></div><div>😃<br></div><br></div>
```

## Installation

#### Requirements :
* Python3 with [mail-parser](https://github.com/SpamScope/mail-parser), [Flask](https://flask.palletsprojects.com/en/2.0.x/), and [aiosmtpd](https://github.com/aio-libs/aiosmtpd/) libraries
* A domain name

#### Setup :
Edit `mailjette.py` to change these variables according to your installation.
* `MYDOMAIN` : your domain name

#### Start :
Launch with `python3 mailjette.py`. It will wait for emails on port 2525 and for HTTP requests on port 8080.
