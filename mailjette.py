from flask import Flask, request, render_template, make_response, Response
from aiosmtpd.controller import Controller
from datetime import datetime
import mailparser
import hashlib
import os
import glob
import pickle
import json
import secrets

MYDOMAIN = "@mailjette.example.com"

application = Flask(__name__)

class MailHandler:
  async def handle_RCPT(self, server, session, envelope, address, rcpt_options):
    if not address.endswith(MYDOMAIN):
      return '550 domain not accepted'
    envelope.rcpt_tos.append(address)
    return '250 OK'

  async def handle_DATA(self, server, session, envelope):
    for rcpt in envelope.rcpt_tos:
      h = hashlib.sha256(pickle.dumps(envelope)).hexdigest()
      os.makedirs("content/"+rcpt+"/"+h[0:3])
      j = json.dumps({"dt": datetime.now().timestamp(), "f": envelope.mail_from, "t": rcpt, "m": envelope.content.decode('utf8', errors='replace')}, indent=2)
      with open("content/"+rcpt+"/"+h[0:3]+"/"+h+".json", "w") as of:
         of.write(j)
    return '250 Message accepted for delivery'

@application.route("/", methods=['GET'])
def mailjette():
  r = make_response(render_template("index.md"))
  r.headers["Content-Type"] = "text/plain"
  return r

def appenddomain(address):
  if not address.endswith(MYDOMAIN):
      address = address+MYDOMAIN
  return address

@application.route("/<address>/list", methods=['GET'])
def listmails(address):
  msglist = []
  address = appenddomain(address)
  for f in glob.glob("content/"+address+"/**/*.json", recursive=True):
    r = {}
    j = open(f, "r")
    c = json.loads(j.read())
    r['id'] = j.name[-69:-5]
    r['datetime'] = datetime.fromtimestamp(c['dt']).strftime("%d/%m/%Y %H:%M:%S")
    r['from'] = c['f']
    msglist.append(r)
    j.close()
  return Response(json.dumps(msglist), mimetype="application/json", status=200)

@application.route("/<address>/delete/<mailid>", methods=['GET'])
def deletemail(address, mailid):
  address = appenddomain(address)
  try:
    os.unlink("content/"+address+"/"+mailid[0:3]+"/"+mailid+".json")
  finally:
    return Response("OK "+mailid+" deleted", mimetype="text/plain", status=200)

@application.route("/<address>/view/<mailid>", methods=['GET'])
def viewmail(address, mailid):
  mimetype = "text/plain"
  msg = ""
  address = appenddomain(address)
  try:
    j = open("content/"+address+"/"+mailid[0:3]+"/"+mailid+".json", "r")
    c = json.loads(j.read())
    mail = mailparser.parse_from_string(c['m'])
    j.close()
    if len(mail.text_html) > 0:
      mimetype = "text/html"
      msg = mail.text_html
    else:
      msg = mail.text_plain
  finally:
    return Response(msg, mimetype=mimetype, status=200)

if __name__ == "__main__":
  cont = Controller(MailHandler(), hostname='', port=2525)
  cont.start()
  application.run(host='::', port=8888)
